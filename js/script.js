// BurgerMenu
(function() {

	"use strict";

	var toggles = document.querySelectorAll(".dropdown-toggle");

	for (var i = toggles.length - 1; i >= 0; i--) {
		var toggle = toggles[i];
		toggleHandler(toggle);
	};

	function toggleHandler(toggle) {
		toggle.addEventListener( "click", function(e) {
			e.preventDefault();
			(this.classList.contains("active") === true) ?
			this.classList.remove("active") :
			this.classList.add("active");
		});
	}

})();

// FancyBox
$(document).ready(function() {
		$(".fancybox").fancybox();
	});

// GoogleMap
function initMap() {
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 13,
    center: {lat: 59.93222, lng: 30.324154},
    scrollwheel: false
  });

  setMarkers(map);
}

var points = [
  ['Fluger', 59.93222, 30.324154],
];

function setMarkers(map) {
  var image = {
    url: 'img/icon-map.png',
   	size: new google.maps.Size(86, 26),
    anchor: new google.maps.Point(75, 26)
  };

  for (var i = 0; i < points.length; i++) {
    var point = points[i];
    var marker = new google.maps.Marker({
      position: {lat: point[1], lng: point[2]},
      map: map,
      icon: image,
      title: point[0]
    });
  }
}






